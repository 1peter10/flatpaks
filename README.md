# Flatpaks

Eventually a few customized JSON files for use with flatpak-builder.


## com.github.rafostar.Clapper.json

The purpose of this is to achieve hardware accelerated video playback with Clapper on the PinePhone and Librem 5, as Brian Daniels [describes here](http://briandaniels.me/2021/07/06/hardware-accelerated-video-playback-on-the-pinephone-with-clapper.html), without having a bunch of files in /usr/local.

It builds Clapper and its gstreamer&co dependencies from git master into a flatpak, depending on release 40 of org.gnome.Platform and org.gnome.Sdk, which you will need to have installed.
You'll also need to have flatpak-builder installed.

Once dependencies are met, just clone this repo or download the .json file and run
~~~~
flatpak-builder --install --user builddir com.github.rafostar.Clapper.json 
~~~~
to build and install Clapper (takes about an hour on the PinePhone).

Big thanks to [Rafostar](https://github.com/Rafostar) and [Brian Daniels](http://briandaniels.me/2021/07/06/hardware-accelerated-video-playback-on-the-pinephone-with-clapper.html) for their hard work that makes this possible!

### Extra performance and frame rate

The following is not enabled by default, as it has let to issues (messed up colors) on some installs in my testing, and I suggest you to use [Flatseal](https://flathub.org/apps/details/com.github.tchx84.Flatseal) to easily remove this added flag in case it does not work for you:

~~~~
flatpak override --user --env=GSK_RENDERER=ngl com.github.rafostar.Clapper 
~~~~

### Early adopters fix

If you tried this before July 13th, 7:20 am UTC, the manifest and the instructions where different, and you had to set the basic environment variables on your own. Sadly, the instruction to set them was flawed and let to terrible performance (see [the issue for reference](https://framagit.org/1peter10/flatpaks/-/issues/1)).

To fix this without a time-consuming rebuilding of the Flatpak, running 
~~~~
flatpak override --user --env=GST_CLAPPER_USE_PLAYBIN3=1 com.github.rafostar.Clapper && flatpak override --user --env=GST_GL_API=gles2 com.github.rafostar.Clapper
~~~~
should be enough. Please report back this does not fix your issue.

### Alternative

Don't have the time to build this, but want hardware accelerated video playback NOW? Try [µPlayer](https://flathub.org/apps/details/org.sigxcpu.Livi) or get a built, optimized version of [Clapper over on this GitHub](https://github.com/uniformbuffer/com.github.rafostar.Clapper/releases/tag/master_pinephone_3bf636d46bfe2b0d432c762859e4ea00aa3e6412)!
